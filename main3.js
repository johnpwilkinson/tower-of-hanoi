let towerClick1;
let towerClick2;
let selectedTower = false;

function clickedTower(e) {
  if (selectedTower) {
    towerClick2 = e.currentTarget;
    moveDisk(towerClick1, towerClick2);
  } else {
    towerClick1 = e.currentTarget;
  }
  selectedTower = !selectedTower;
}

function moveDisk(towerClick1, towerClick2) {
    //if second tower does contain disks
  if (towerClick2.childElementCount > 0) {
    if ( // prevents illegal move by confirming that the last child on tower 2 is woder than that of tower1 
      towerClick1.lastElementChild.innerHTML <
      towerClick2.lastElementChild.innerHTML
    ) { // moves disk from tower1 to tower 2
      towerClick2.appendChild(towerClick1.lastElementChild);
    }//if 2nd tower doesnt contain any disks
  } else {
    towerClick2.appendChild(towerClick1.lastElementChild);
  } //checks for winning condition
  check4win();
}

function check4win() {
  if (tower1.childElementCount === 0 && tower2.childElementCount === 0) {
    const winBox = document.getElementById("winner");
    const winText = document.createTextNode("you win");
    winBox.classList.add("H1");
    winBox.appendChild(winText);
  }
}

let tower1 = document.getElementById("tA");
let tower2 = document.getElementById("tB");
let tower3 = document.getElementById("tC");

tower1.addEventListener("click", clickedTower);
tower2.addEventListener("click", clickedTower);
tower3.addEventListener("click", clickedTower);
